package ctf

import "strings"

// Bytes2D holds a 2D array of bytes.
type Bytes2D [][]byte

// NewBytes2D initializes a new 2D array with the given size.
func NewBytes2D(x, y int) Bytes2D {
	b := make(Bytes2D, y)

	for y := range b {
		b[y] = make([]byte, x)
	}

	return b
}

// FlipHorizontal flips the 2D array horizontally.
func (b Bytes2D) FlipHorizontal() Bytes2D {
	ret := NewBytes2D(b.Width(), b.Height())

	b.Walk(func(x, y int, char byte) {
		ret[y][ret.Width()-1-x] = char
	})

	return ret
}

// FlipVertical flips the 2D array vertically.
func (b Bytes2D) FlipVertical() Bytes2D {
	ret := NewBytes2D(b.Width(), b.Height())

	b.Walk(func(x, y int, char byte) {
		ret[b.Height()-1-y][x] = char
	})

	return ret
}

// RotateRight rotates the Bytes2D 90 degrees right.
func (b Bytes2D) RotateRight() Bytes2D {
	ret := NewBytes2D(b.Width(), b.Height())

	b.Walk(func(x, y int, char byte) {
		ret[x][ret.Width()-1-y] = char
	})

	return ret
}

// RotateLeft rotates the Bytes2D 90 degrees left.
func (b Bytes2D) RotateLeft() Bytes2D {
	ret := NewBytes2D(b.Width(), b.Height())

	b.Walk(func(x, y int, char byte) {
		ret[b.Height()-1-x][y] = char
	})

	return ret
}

// String serializes the 2D array to a string.
func (b Bytes2D) String() string {
	var builder strings.Builder

	for _, row := range b {
		builder.Write(row)
		builder.WriteByte('\n')
	}

	return builder.String()
}

// CharAt returns the byte at the given coordinates, or the zero byte if out of
// bounds.
func (b Bytes2D) CharAt(x, y int) byte {
	if x >= b.Width() ||
		y >= b.Height() {
		return 0
	}

	return b[y][x]
}

// Width returns the width of the 2D array.
func (b Bytes2D) Width() int {
	if len(b) == 0 {
		return 0
	}

	return len(b[0])
}

// Height returns the height of the 2D array.
func (b Bytes2D) Height() int {
	return len(b)
}

// Equal determines whether the 2 Bytes2D are equal.
func (b Bytes2D) Equal(other Bytes2D) bool {
	if b.Height() != other.Height() ||
		b.Width() != other.Width() {
		return false
	}

	isEqual := true

	b.Walk(func(x, y int, char byte) {
		if other.CharAt(x, y) != char {
			isEqual = false
		}
	})

	return isEqual
}

// FindNumChars finds the total number of the given char.
func (b Bytes2D) FindNumChars(char byte) int {
	var count int

	b.Walk(func(_, _ int, current byte) {
		if current == char {
			count++
		}
	})

	return count
}

// FindNumBytes2D finds all instances of the given Bytes2D contained within the
// larger Bytes2D.
func (b Bytes2D) FindNumBytes2D(sub Bytes2D) int {
	var count int

	b.Walk(func(x, y int, _ byte) {
		foundMatch := true
		for subY, subRow := range sub {
			for subX, subPixel := range subRow {
				if y+subY >= b.Height() {
					foundMatch = false
					continue
				}

				if x+subX >= b.Width() {
					foundMatch = false
					continue
				}

				if b[y+subY][x+subX] != subPixel {
					foundMatch = false
				}
			}
		}

		if foundMatch {
			count++
		}
	})

	return count
}

// Walk walks through each element of the 2D array, giving the location and
// character of each.
func (b Bytes2D) Walk(fn func(x, y int, char byte)) {
	for y, row := range b {
		for x, char := range row {
			fn(x, y, char)
		}
	}
}
